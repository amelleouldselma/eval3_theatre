## Bienvenue sur le site du club de théatre la "Société des beaux-parleurs"  

## Lien vers mon Kanban (Asana)
https://app.asana.com/0/1199967423099983/board 

## Vous pourez retrouvez les diagrammes dans le dossier "diagrammes"

 ## Pour l'installation en local:

- git clone git@gitlab.com:amelleouldselma/eval3_theatre.git
- cd eval3_theatre
- Installer les composants avec la commande : "composer install" et "npm install",


- Crée un fichier .env en prenant pour exemple le fichier .env.example,
- Modifier  DB_DATABASE, DB_USERNAME, DB_PASSWORD,
- "php artisan serve" pour ouvir avec votre serveur local.
   
## La mise en ligne a ete faite avec Heroku, cependant j'ai été bloquer lorsqu'il a fallut migrate mes tables avec heroku, je n'ai pas reussi a reoudre l'erreur.
 
 https://ancient-ridge-50187.herokuapp.com/

## J'ai donc essayer d'une autre facon mais en vain. J'ai essayer de migrer mes tables grace à sqlelectron. Je pense aussi avoir une erreur de migration car lors d'une connection il y a une erreur.

http://theatre-beauxparleurs.herokuapp.com

## N'hésitez pas à visitez le site en local, tout fonctionne .. :)