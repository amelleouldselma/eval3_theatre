<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sujet;
use Illuminate\Support\Facades\DB;


class RouletteController extends Controller
{

   public function getSujet(){
        
   return view('roulette', ['sujets' => Sujet::all()->random(1)[0]]);
     
   //return view('roulette', ['sujets' => Sujet::where('status', 'todo')->inRandomOrder()->first()->sujet]);
   }

   public function new(Request $request){
       DB::table('sujets')->where('id',$request->id)->update(['status' => 'done']);
       return redirect('/subject');
   }
}
