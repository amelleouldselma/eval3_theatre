<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RandomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sujets')->insert([
            'sujet' => "Si nous pouvions respirer sous l'eau…",
        ]);
        DB::table('sujets')->insert([
            'sujet' => "Aujourd'hui, c'est le premier jour d'école.",
        ]);
        DB::table('sujets')->insert([
            'sujet' => "Deux familles se croisent au concours de barbecue de Dunkerque.",
        ]);
        DB::table('sujets')->insert([
            'sujet' => "Un nouveau colocataire dans l'appart",
        ]);
        DB::table('sujets')->insert([
            'sujet' => "L’ascenseur est en panne.",
        ]);
        DB::table('sujets')->insert([
            'sujet' => "Vous présentez une émission culinaire et proposez une recette originale",
        ]);
        DB::table('sujets')->insert([
            'sujet' => "Vous arrivez en cours : la classe est vide, pas de professeur, pas de camarades..." ,
        ]);
    }
}
