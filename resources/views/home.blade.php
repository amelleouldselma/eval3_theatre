@extends('layouts.app')


@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Bonjour {{ Auth::user()->name}} </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container mb-5" style="
    box-shadow: 0 8px 32px 0 rgba( 31, 38, 135, 0.37 );
    backdrop-filter: blur( 6.0px );
    -webkit-backdrop-filter: blur( 6.0px );
    border-radius: 10px;
    border: 3px solid #e1eef7;">
        <h4 class=" p-5">Bienvenue chez la Société des beaux-parleurs, le club d'improvisation théâtrale.
                        Nous nous réunissons tous les jeudis pour des impros de folies. Vous avez la possibilité de proposer des sujets sur ce site, n'hésitez pas !
                        Le sujet est alors lancer sur la page Roulette, nos membres ont la possibilité de nous indiquer si les sujets ont été déjà jouer.
        </h4>

</div>



<div class="container">
  <div class="row">
    <div class="col">
    </div>
    <div class="col">
        <form action="{{route('subject')}}" method="POST">
         @csrf
            <p>Proposez nous un sujet pour le battle de théatre!<input id="input" type="text" name="proposition" /></p>
            <p><input type="submit" value="OK"></p>
        </form>
    </div>
    <div class="col">
    </div>
  </div>
</div>

@endsection
