@extends('layouts.app')

@section('content')
    <body>

        <div class="row">
            <div class="col-6 col-md-4"><div class="card shadow-lg p-3 mb-5 bg-white rounded" style="width: 30rem">
            <div class="card-body">
                <h5 class="card-title"><h2 style="color:#8E24AA">Les sujets jamais jouer :</h2></h5>
                <p class="card-text">
                    @foreach($sujets as $sujet)
                        @if($sujet->status === 'todo')
                            <p>- {{$sujet->sujet}}</p>
                        @endif
                    @endforeach        
                </p>
            </div>
        </div></div>
            <div class="col-6 col-md-4"></div>
            <div class="col-6 col-md-4"><div class="card shadow-lg p-3 mb-5 bg-white rounded" style="width: 30rem">
            <div class="card-body">
                <h5 class="card-title"><h2 style="color:#8E24AA">Les sujets déja jouer :</h2></h5>
                <p class="card-text">
                    @foreach($sujets as $sujet)
                        @if($sujet->status === 'done')
                            <p>- {{$sujet->sujet}}</p>
                        @endif
                    @endforeach        
                </p>
            </div>
        </div></div>
        </div>


        <div class="card text-center shadow-lg p-3 mb-5 bg-white rounded">
            <div class="card-header"><h2 style="color:#8E24AA">Tout les sujets :</h2></div>
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <p class="card-text">
                        @foreach ($sujets as $sujet)
                            <p>-{{$sujet->sujet}}</p>
                        @endforeach
                    </p>
                    <a href={{"/home"}} class="btn btn-primary" style="background-color:#8E24AA">Proposez nous un sujet ;)</a>
                </div>
        </div>

    </body>
@endsection
