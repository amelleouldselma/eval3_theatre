@extends('layouts.app')

@section('content')

    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading">Merci !</h4>
        <p>Votre sujet est enregistrer et serat bientot jouer chez les beaux-parleurs.</p>
        <hr>
        <p class="mb-0"><a href="{{route('/home')}}" class="btn btn-dark my-5">Proposer nous un autre sujet.</a></p>
    </div>

@endsection
