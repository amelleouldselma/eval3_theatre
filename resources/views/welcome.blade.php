@extends('layouts.app')

@section('content')
<body>

<div class="container-fluid">

    <h1 class="text-white p-4" style="font-family: 'Abril Fatface', cursive !important; font-size: 50px"> <a class="text-decoration-none" style="color: white;" href="{{ url('/') }}">TchiTcha</a></h1>

    <div class="container mb-5" style="
    background: #9abfd9;
    box-shadow: 0 8px 32px 0 rgba( 31, 38, 135, 0.37 );
    backdrop-filter: blur( 6.0px );
    -webkit-backdrop-filter: blur( 6.0px );
    border-radius: 10px;
    border: 3px solid #e1eef7;">
        <h1 class="text-white p-5">Bienvenue chez la Société des beaux-parleurs, nous sommes un club d'improvisation théatrale.</h1>


            <div class="col-sm">
                <h4 class="text-white p-3 rounded-3" ><a class="text-decoration-none" href="{{ route('register') }}">Rejoignez le club !</a></h4>
                <h4 class="text-white p-3 rounded-3" ><a class="text-decoration-none" href="{{ route('login') }}">Vous êtes membres du club ? Connectez-vous.</a></h4>
            </div>
    </div>
</div>

</body>
@endsection

