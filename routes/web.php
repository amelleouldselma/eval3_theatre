<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SubjectController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});

Route::get('/roulette', function (){
    return view('/roulette');
});

Route::get('/roulette', [App\Http\Controllers\RouletteController::class, 'getSujet'])->name('roulette');

Route::get('/roulette/{id}', [App\Http\Controllers\RouletteController::class, 'new']);

Route::get('/subject', function (){
    return view('/subject');
});

Route::post('/subject', [App\Http\Controllers\HomeController::class, 'saveDb'])->name('input');

Route::get('/subject', [App\Http\Controllers\SubjectController::class, 'all'])->name('subject');

Route::get('/thanks', function (){
    return view('/thanks');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
